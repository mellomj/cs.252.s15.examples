#include <iostream>
#include <list>
#include <stack>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

#define DO_BINARY_OP(_op_) \
    op1 = s.top(); s.pop(); \
    op2 = s.top(); s.pop(); \
    s.push(op2 _op_ op1);

int
main(int argc, char *argv[])
{
    string line;

    while (getline(cin, line)) {
    	stringstream ss(line);
    	string token;
    	stack<int, vector<int>> s;
    	int op1, op2;

    	while (ss >> token) {
    	    if (token == "+") {
    	    	DO_BINARY_OP(+)
	    } else if (token == "-") {
    	    	DO_BINARY_OP(-)
	    } else if (token == "*") {
    	    	DO_BINARY_OP(*)
	    } else if (token == "/") {
    	    	DO_BINARY_OP(/)
	    } else {
	    	s.push(stoi(token));
	    }
	}

	cout << s.top() << endl;
    }

    return EXIT_SUCCESS;
}
