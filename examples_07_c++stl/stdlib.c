/* stdlib.c: demonstrate qsort an bsearch */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAX_ITEMS 8

void
print_array(int *a, int n)
{
    for (int i = 0; i < n; i++) {
    	printf("%d: %d\n", i, a[i]);
    }
}

int
int_compare(const void *a, const void *b)
{
    const long ia = *(const long *)(a);
    const long ib = *(const long *)(b);

    return (int)(ia - ib);
}

int
main(int argc, char *argv[])
{
    int data[MAX_ITEMS];

    /* Seed random number generator */
    srand(time(NULL));

    /* Initialize data array with random number */
    for (int i = 0; i < MAX_ITEMS; i++) {
    	data[i] = rand() % MAX_ITEMS;
    }

    print_array(data, MAX_ITEMS);
    
    /* Sort array */
    qsort(data, MAX_ITEMS, sizeof(int), int_compare);

    puts("Sorted!");
    print_array(data, MAX_ITEMS);
    
    /* Search array */
    for (int i = 0; i < MAX_ITEMS; i++) {
    	printf("Is %d in data? %d\n", i, bsearch(&i, data, MAX_ITEMS, sizeof(int), int_compare) != NULL);
    }
}
