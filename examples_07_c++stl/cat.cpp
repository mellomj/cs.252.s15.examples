#include <algorithm>
#include <iostream>
#include <iterator>
#include <string>

using namespace std;

struct Line {
    string data;
    operator string() const { return data; }
};
    
istream &operator>>(istream &is, Line &l)
{
    return getline(is, l.data);
}

int
main(int argc, char *argv[])
{
    copy(istream_iterator<Line>(cin), istream_iterator<Line>(), ostream_iterator<string>(cout, "\n"));
    return EXIT_SUCCESS;
}
