/* isprime.c: check if number is prime */

#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

bool
is_prime(int n)
{
    if (n > 2 && n % 2 == 0) {
    	return false;
    }

    for (int i = 3; i < sqrt(n); i += 2) {
    	if (n % i == 0) {
    	    return false;
	}
    }

    return true;
}

int
main(int argc, char *argv[])
{
    if (argc == 1) {
    	fprintf(stderr, "usage: %s numbers...\n", argv[0]);
    	return EXIT_FAILURE;
    }

    for (int i = 1; i < argc; i++) {
    	int n = atoi(argv[i]);

    	printf("IsPrime(%d)? %s\n", n, is_prime(n) ? "Yes" : "No");
    }

    return 0;
}
