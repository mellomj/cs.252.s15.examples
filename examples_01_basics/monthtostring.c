/* monthtostring.c */

#include <stdio.h>
#include <stdlib.h>

int
main(int argc, char *argv[])
{
    int month;

    if (argc != 2) {
    	fprintf(stderr, "usage: %s month\n", argv[0]);
    	return (EXIT_FAILURE);
    }

    month = atoi(argv[1]);
    switch (month) {
	case 1:	    puts("January");    break;
	case 2:	    puts("February");   break;
	case 3:	    puts("March");	break;
	case 4:	    puts("April");	break;
	case 5:	    puts("May");	break;
	case 6:	    puts("June");	break;
	case 7:	    puts("July");	break;
	case 8:	    puts("August");	break;
	case 9:	    puts("September");  break;
	case 10:    puts("October");	break;
	case 11:    puts("November");	break;
	case 12:    puts("December");	break;
	default:
	    fprintf(stderr, "Unknown month %d\n", month);
	    exit(EXIT_FAILURE);
    }

    return (EXIT_SUCCESS);
}
