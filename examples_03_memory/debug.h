#ifndef __DEBUG_H__
#define __DEBUG_H__

#include <stdio.h>

#ifdef DEBUG
#define debug(M, ...) fprintf(stderr, "DEBUG %s:%d: " M "\n", __FILE__, __LINE__, ##__VA_ARGS__)
#else
#define debug(M, ...)
#endif

#define warn(M, ...)  fprintf(stderr, "WARN  %s:%d: " M "\n", __FILE__, __LINE__, ##__VA_ARGS__)

#endif
