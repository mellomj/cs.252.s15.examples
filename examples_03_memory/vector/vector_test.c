#include "vector.h"

static const int VectorData[] = {5, 4, 7, 0, 1};

void
test_vector_create()
{
    struct vector_t *v;

    v = vector_create(0, 0);
    vector_print(v);
    vector_delete(v);
    
    v = vector_create(4, 4);
    vector_print(v);
    vector_delete(v);
}

void
test_vector_append()
{
    struct vector_t *v;

    v = vector_create(0, 2);
    for (size_t i = 0; i < sizeof(VectorData)/sizeof(int); i++) {
    	vector_append(v, VectorData[i]);
    }

    vector_print(v);
    vector_delete(v);
}

int
main(int argc, char *argv[])
{
    test_vector_create();
    test_vector_append();

    return EXIT_SUCCESS;
}
