#include <stdio.h>
#include <stdlib.h>

int
main(int argc, char *argv[])
{
    int array[] = {0, 1, 2, 3, -1};

    for (int i = 0; array[i] >= 0; i++) {
    	printf("array[%d] = %d\n", i, array[i]);
    }
    putchar('\n');
    
    for (int i = 0; *(array + i) >= 0; i++) {
    	printf("array[%d] = %d\n", i, *(array + i));
    }
    putchar('\n');
    
    for (int *p = array; *p >= 0; p++) {
    	printf("array[%ld] = %d\n", p - array, *p);
    }

    return EXIT_SUCCESS;
}
