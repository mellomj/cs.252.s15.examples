#include <stdio.h>
#include <stdlib.h>

#define PRINT_BITWISE_BINARY_TABLE(name, op)	    \
    puts(name);					    \
    for (int x = 0; x < 2; x++)			    \
	for (int y = 0; y < 2; y++)		    \
	    printf("%d %d || %d\n", x, y, x op y);  \

#define PRINT_BITWISE_UNARY_TABLE(name, op)	    \
    puts(name);					    \
    for (int x = 0; x < 2; x++)			    \
	printf("%d || %d\n", x, (op x) & 0x1);	    \

#define PRINT_BITWISE_SHIFT_TABLE(name, op)	    \
    puts(name);					    \
    for (int x = 0; x < 8; x++)			    \
	printf("%d || %d\n", x, 0xff op x);	    \

int
main(int argc, char *argv[])
{
    PRINT_BITWISE_BINARY_TABLE("OR", |);
    PRINT_BITWISE_BINARY_TABLE("AND", &);
    PRINT_BITWISE_BINARY_TABLE("XOR", ^);
    
    PRINT_BITWISE_UNARY_TABLE("NOT", ~);
    
    PRINT_BITWISE_SHIFT_TABLE("LEFT", <<);
    PRINT_BITWISE_SHIFT_TABLE("RIGHT", >>); 

    return EXIT_SUCCESS;
}
