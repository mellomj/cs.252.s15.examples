/* xcrypt.c: simple xor encryption and decryption */

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

char Key = SCHAR_MAX;
    
void
xcrypt(FILE *fp)
{
    char buffer[BUFSIZ];

    while (fgets(buffer, BUFSIZ, fp) != NULL) {
	for (char *bp = buffer; *bp; bp++) {
	    putchar(*bp ^ Key);
	}
    }
}

int
main(int argc, char *argv[])
{
    int c;

    while ((c = getopt(argc, argv, "k:")) != -1) {
    	switch (c) {
	    case 'k':
	    	Key = atoi(optarg);
	    	break;
	    default:
	    	fprintf(stderr, "%s: unknown arg %c\n", argv[0], c);
	    	return EXIT_FAILURE;
	}
    }

    xcrypt(stdin);

    return EXIT_SUCCESS;
}
