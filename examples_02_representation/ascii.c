#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

int
main(int argc, char *argv[])
{
    for (int c = 0; c < CHAR_MAX; c++) {
    	printf("%03d: %c\n", c, c);
    }

    return EXIT_SUCCESS;
}
