#include <stdio.h>
#include <stdlib.h>

int
main(int argc, char *argv[])
{
    FILE *process;
    char  buffer[BUFSIZ];
    
    process = popen("ls -l", "r");
    if (process == NULL) {
    	perror("popen");
    	return EXIT_FAILURE;
    }

    for (int i = 0; fgets(buffer, BUFSIZ, process); i++) {
    	printf("%8d: %s", i, buffer);
    }

    return EXIT_SUCCESS;
}
