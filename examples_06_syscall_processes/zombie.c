/* zombie: fork */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int
main(int argc, char *argv[])
{
    pid_t pid;

    pid = fork();
    switch (pid) {
	/* Error */
	case -1:
	    perror("fork");
	    exit(EXIT_FAILURE);
	    break;

	/* Child */
	case 0:
	    printf("[CHILD] pid=%d\n", getpid());
	    _exit(EXIT_SUCCESS);
	    break;

	/* Parent */
	default:
	    while (true) {
		printf("[PARENT] pid=%d (not waiting...)\n", pid);
		sleep(1);
	    }
	    break;
    }

    return (EXIT_SUCCESS);
}
