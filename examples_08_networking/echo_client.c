/* echo_client.c: simple TCP echo client */

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

int
main(int argc, char *argv[])
{
    /* Parse command line options */
    if (argc != 3) {
	fprintf(stderr, "usage: %s host port\n", argv[0]);
	return EXIT_FAILURE;
    }

    char *host = argv[1];
    char *port = argv[2];

    /* Lookup server address information */
    struct addrinfo *results;
    struct addrinfo  hints;
    int    socket_fd = -1;

    memset(&hints, 0, sizeof(hints));
    hints.ai_family   = AF_UNSPEC;	/* Use either IPv4 (AF_INET) or IPv6 (AF_INET6) */
    hints.ai_socktype = SOCK_STREAM;	/* Use TCP  */

    if (getaddrinfo(host, port, &hints, &results) < 0) {
    	fprintf(stderr, "Could not look up %s:%s: %s\n", host, port, strerror(errno));
    	goto failure;
    }

    /* For each server entry, allocate socket and try to connect */
    for (struct addrinfo *p = results; p != NULL; p = p->ai_next) {
	char ip[INET6_ADDRSTRLEN];

	/* Translate IP address to string */
	if (p->ai_family == AF_INET) {
	    inet_ntop(p->ai_family, &(((struct sockaddr_in *)p->ai_addr)->sin_addr), ip, sizeof(ip));
	} else {
	    inet_ntop(p->ai_family, &(((struct sockaddr_in6 *)p->ai_addr)->sin6_addr), ip, sizeof(ip));
	}

	/* Allocate socket */
	if ((socket_fd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) < 0) {
	    fprintf(stderr, "Unable to make socket: %s\n", strerror(errno));
	    continue;
	}

	/* Connect to host */
	if (connect(socket_fd, p->ai_addr, p->ai_addrlen) < 0) {
	    fprintf(stderr, "Unable to connect to %s:%s: %s\n", ip, port, strerror(errno));
	    close(socket_fd);
	    continue;
	}

	/* Successful connection */
	fprintf(stderr, "Connected to %s:%s\n", ip, port);
	break;
    }
    freeaddrinfo(results);

    /* Open and return FILE from socket */
    FILE *socket_file = fdopen(socket_fd, "r+");
    if (socket_file == NULL) {
    	fprintf(stderr, "Could not open socket as FILE: %s\n", strerror(errno));
    	goto failure;
    }

    /* Send stdin to server and write results to stdout */
    char buffer[BUFSIZ];

    while (true) {
	printf("> ");
	fflush(stdout);

	/* Read from stdin */
	if (fgets(buffer, BUFSIZ, stdin) == NULL) {
	    break;
	}

	/* Write to to server */
	if (fputs(buffer, socket_file) == EOF) {
	    break;
	}

	/* Read from server */
	if (fgets(buffer, BUFSIZ, socket_file) == NULL) {
	    break;
	}

	/* Write to stdout */
	fputs(buffer, stdout);
    }

    fclose(socket_file);

    return EXIT_SUCCESS;

failure:
    return EXIT_FAILURE;
}
